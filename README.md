# portail

Site web des Cantons Solidaires Autonomes

## Développement

- [Hugo](https://gohugo.io/documentation)
- [Codex](https://github.com/jakewies/hugo-theme-codex)

### Installation

Configurer une [clé SSH](https://framagit.org/help/ssh/README.md)
et [cloner](https://git-scm.com/docs/git-clone) le dépôt :

```bash
git clone git@framagit.org:cantons/portail.git

cd portail
```

Générer le site dans le dossier `public` :

```bash
hugo
```

Ou avec [Docker Compose](https://docs.docker.com/compose/) :

```bash
echo 'HUGO_BASE_URL=https://example.com' >> .env

docker-compose up --build -d
```

### Utilisation

Hugo permet de définir un statut "brouillon" avec [front matter](https://gohugo.io/content-management/front-matter/).
Par défaut, un contenu avec le statut `draft: true` [ne sera pas publié](https://gohugo.io/getting-started/usage/#draft-future-and-expired-content).

#### Ajout d'un article

```bash
SLUG=example
hugo new articles/$SLUG.md
```

Brouillon par défaut.

#### Ajout d'un canton

```bash
DEPT=01
SLUG=example
hugo new cantons/$DEPT/$SLUG/index.md
```

Ajouter une image et compléter le fichier, brouillon par défaut.

#### Ajout d'un partenaire

```bash
SLUG=example
hugo new partenaires/$SLUG/index.md
```

Ajouter le logo et compléter le fichier, brouillon par défaut.

#### Shortcodes

##### Adresses

```md
{{< cloak "contact@example.com" >}}
{{< cloak address="user@example.com" protocol="xmpp" >}}
```

##### Images

```md
![Texte alternatif](example.jpg)
{{< figure "example.jpg" "Texte alternatif" "Description" >}}
{{< figure src="example.jpg" alt="Texte alternatif" caption="Description" >}}
```

##### Icones

```md
{{< icon "example" >}}
{{< icon slug="example" url="https://example.com" title="Exemple avec lien" >}}
```

Logo : `assets/svg/example.svg`

##### Amis

```yaml
---
friends:
- title: Example
  url: https://example.com
  src: https://example.com/logo.png
---

{{< friends >}}
```

##### Réseaux

```yaml
---
social:
- title: Example
  url: https://example.com
---
```

Logo : `assets/svg/example.svg`

##### Vidéos

```md
{{< peertube "peertube.example.com" "id" >}}
{{< peertube host="peertube.example.com" id="id" >}}
```

## Générer les pages de canton

### Prérequis

- Go 1.13

### Commande

<!--
go get gopkg.in/yaml.v2
go get github.com/JohannesKaufmann/html-to-markdown
go get golang.org/x/text/runes
go get golang.org/x/text/unicode/norm
-->

- Lancer les commandes :
```bash
find content/cantons -mindepth 1 -type d -exec rm -r {} \;
go run scripts/cantons.go
```
- Puis ajouter à git les pages générées.

## Idées

- [ ] Dark mode
- [ ] Print CSS
- [ ] Recherche
