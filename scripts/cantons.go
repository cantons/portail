package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
	"unicode"

	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"

	md "github.com/JohannesKaufmann/html-to-markdown"
	"gopkg.in/yaml.v2"
)

type Response struct {
	Licence, Ontology string
	Data              []Canton
}

type Canton struct {
	ID, Name string
	Geo      struct {
		Latitude, Longitude float64
	}
	Address struct {
		StreetAddress, AddressLocality, PostalCode, AddressCountry, CustomFormatedAddress string
	}
	CreatedAt, UpdatedAt, Date string
	Status                     int
	Categories                 []string
	CategoriesFull             []struct {
		ID, Index         int
		Name, Description string
	}
	Description, Presentation, Skills, Tags, Slug string
	Participants                                  interface{}
	Images                                        []string
	Email                                         string
	Site_web, Site_web_name                       string
	Friend_name, Friend_url, Friend_img           string
        GoGoCartoURL                                  string
	// Contributions
}

type Resource struct {
	Name, Src string
}

type Link struct {
	Title, URL string
}

type Friend struct {
	Title, URL, Src string
}

type Page struct {
	Type, Layout, Title, URL             string
	Aliases                              []string
	Date, Description                    string
	Resources                            []Resource
	Keywords, Tags, Regions, Competences []string
	Participants                         int
	Social                               []Link
	Friends                              []Friend
	Toc                                  bool
	Lat, Lon                             float64
}

var (
	cantonsDir   = "cantons"
	contentDir   = filepath.Join("content", cantonsDir)
	gogocartoURL = "https://cantons.gogocarto.fr/api/elements.json"
	httpClient   = &http.Client{Timeout: 10 * time.Second}
)

func getJSON(url string, v interface{}) error {
	r, err := httpClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(v)
}

func (p *Page) parseURL(el *Canton) *Page {
	prefix := "/cantons"
	url := prefix
	if el.Address.PostalCode != "" {
		url += "/" + el.Address.PostalCode
	}
	slug := strings.ReplaceAll(el.Name, " ", "-")
	slug = strings.ReplaceAll(slug, "/", "-")
	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	result, _, _ := transform.String(t, slug)
	el.GoGoCartoURL = "https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/" + result + "/" + el.ID + "/"
	fmt.Println(result, el.GoGoCartoURL)
	slug = strings.ToLower(result)
	url += "/" + slug
	if el.Slug != "" {
		p.URL = prefix + "/" + el.Slug
	} else {
		p.URL = url
	}
	if url != p.URL {
		p.Aliases = append(p.Aliases, url)
	}
	return p
}

func (p *Page) getContentPath(el Canton) string {
	dir := strings.TrimPrefix(filepath.FromSlash(p.URL), "/")
	return filepath.Join("content", dir)
}

func (p *Page) parseImage(el Canton) *Page {
	if len(el.Images) > 0 {
		// Download
		url := el.Images[0]
		dir := p.getContentPath(el)
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			panic(err)
		}
		ext := strings.Replace(filepath.Ext(url), "jpeg", "jpg", -1)
		src := "image" + ext
		dst := filepath.Join(dir, src)
		r, err := http.Get(url)
		if err != nil {
			panic(err)
		}
		defer r.Body.Close()
		f, err := os.Create(dst)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		_, err = io.Copy(f, r.Body)
		if err != nil {
			panic(err)
		}
		// fmt.Println("Downloaded", dst)
		p.Resources = append(p.Resources, Resource{Name: "featured", Src: src})
	}
	return p
}

func (p *Page) parseFriends(el Canton) *Page {
	if el.Friend_name != "" {
		p.Friends = append(p.Friends, *&Friend{
			Title: el.Friend_name,
			URL:   el.Friend_url,
			Src:   el.Friend_img,
		})
	}
	return p
}

func (p *Page) parseParticipants(el Canton) *Page {
	if el.Participants != "" {
		if i, err := strconv.Atoi(fmt.Sprint(el.Participants)); err == nil {
			p.Participants = i
		}
	}
	return p
}

func (p *Page) parseSocial(el Canton) *Page {
	if (el.Site_web != "") && (el.Site_web_name != "") {
		p.Social = append(p.Social, Link{Title: el.Site_web_name, URL: el.Site_web})
	}
	if (el.Email != "") && (el.Email != "private") {
		p.Social = append(p.Social, Link{Title: "email", URL: fmt.Sprintf("mailto:%s", el.Email)})
	}
	return p
}

func appendIfMissing(slice []string, s string) []string {
	for _, element := range slice {
		if strings.ToLower(element) == strings.ToLower(s) {
			return slice
		}
	}
	return append(slice, s)
}

func (p *Page) parseTags(el Canton) *Page {
	tags := []string{}
	if el.Tags != "" {
		for _, t := range strings.Split(el.Tags, ",") {
			tag := strings.TrimSpace(t)
			tags = append(tags, tag)
		}
	}
	if el.Address.PostalCode != "" {
		postalCode := strings.TrimSpace(el.Address.PostalCode)
		if len(el.Address.PostalCode) == 5 {
			postalCode = el.Address.PostalCode[0:2]
		}
		tags = appendIfMissing(tags, postalCode)
	}
	if el.Address.AddressLocality != "" {
		tags = appendIfMissing(tags, strings.TrimSpace(el.Address.AddressLocality))
	}
	for _, t := range tags {
		p.Regions = append(p.Regions, t)
		p.Keywords = append(p.Keywords, t)
	}
	return p
}

func (p *Page) parseSkills(el Canton) *Page {
	if el.Skills == "" {
		return p
	}
	skills := strings.Split(el.Skills, ",")
	for _, s := range skills {
		str := strings.TrimSpace(s)
		p.Competences = append(p.Competences, str)
		p.Keywords = append(p.Keywords, str)
	}
	return p
}

func generatePage(el Canton) error {
	date := el.CreatedAt
	if el.Date != "" {
		date = el.Date
	}
	p := Page{
		Type:        "pages",
		Layout:      "canton",
		Title:       el.Name,
		Date:        date,
		Description: el.Description,
		Toc:         true,
		Lat:         el.Geo.Latitude,
		Lon:         el.Geo.Longitude,
	}
	p.parseURL(&el).parseImage(el).parseParticipants(el).parseSocial(el).parseFriends(el).parseTags(el).parseSkills(el)
	d, err := yaml.Marshal(&p)
	if err != nil {
		return err
	}
	str := "---\n" + string(d) + "---\n"
	if el.Presentation != "" {
		converter := md.NewConverter("", true, nil)
		markdown, err := converter.ConvertString(el.Presentation)
		if err != nil {
			return err
		}
		markdown = strings.ReplaceAll(markdown, "\\#", "#")
		markdown = strings.ReplaceAll(markdown, "\\-", "-")
		str += "\n## Présentation\n"
		str += "\n" + markdown + "\n"
	}
	if len(el.Skills) > 0 {
		str += "\n## Compétences\n"
		str += "\n{{< skills >}}\n"
	}
	if el.Address.CustomFormatedAddress != "" {
		str += "\n## Adresse\n"
		str += "\n" + el.Address.CustomFormatedAddress + "\n"
		// str += `<div id="map" class="osm"></div>` + "\n"
		str += `{{< iframe url="` + el.GoGoCartoURL + `" id="map" >}}` + "\n"

	}
	if el.Site_web != "" {
		name := el.Site_web_name
		if name == "" {
			name = "Site web"
		}
		str += "\n## " + name + "\n"
		str += "\n{{< social >}}\n"
		str += "\n" + `<center><a href="` + el.Site_web + `" target="_blank">` + el.Site_web + `</a></center>` + "\n"
	}
	dir := p.getContentPath(el)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		return err
	}
	dst := filepath.Join(dir, "index.md")
	if err := ioutil.WriteFile(dst, []byte(str), 0644); err != nil {
		return err
	}
	return nil
}

func main() {
	res := Response{}
	if err := getJSON(gogocartoURL, &res); err != nil {
		log.Fatal(err)
	}
	if err := os.MkdirAll(contentDir, os.ModePerm); err != nil {
		log.Fatal(err)
	}
	for _, el := range res.Data {
		if el.Status == 0 {
			continue
		}
		fmt.Printf("%s (%s) %s [%v]\n", el.Name, el.ID, el.Address.PostalCode, el.Tags)
		if err := generatePage(el); err != nil {
			log.Fatal(err)
		}
	}
}
