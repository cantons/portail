---
title: "Les Crapauds Fous"
date: 2021-03-22T00:00:00+00:00
description: "Face aux dangers qui pèsent sur notre avenir, seul un grain de folie peut nous sortir de l'ornière (et de la déprime passive).
En avril 2017, une trentaine de personnes se reunissent aux Treilles dans le Var et donnent naissance au manifeste des Crapauds Fous."
link: https://crapaud-fou.org/
resources:
- name: featured
  src: les-crapauds-fous.svg
  title:
keywords: []
tags: []
toc: false
social:
- title: Email
  cloak:
    address: contact a crapaud-fou.org
---

Face aux dangers qui pèsent sur notre avenir, seul un grain de folie peut nous sortir de l'ornière (et de la déprime passive).

En avril 2017, une trentaine de personnes se reunissent aux Treilles dans le Var et donnent naissance au manifeste des Crapauds Fous.
