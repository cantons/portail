---
title: "Canal Concorde"
date: 2021-03-10T00:00:00+01:00
slug:
description: ""
link: https://www.youtube.com/c/canalconcorde
resources:
- name: featured
  src: canal-concorde.jpg
  title:
keywords: []
tags: []
toc: false
---

Canal Concorde vous propose d'aborder de réfléchir au rôle de la presse, à sa liberté d'expression, de conscience, son droit de penser, d'écrire de diffuser, de son indépendance, qui sont et devraient être les droits les plus précieux dans une démocratie digne de ce nom.

Né du mouvement gilet jaune du 17 novembre 2018, Canal Concorde vous propose chaque mercredi soir à 21h un débat autour de l'histoire, d'alternatives de changement, de démocratie, de politique, d'économie...
