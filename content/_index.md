---
title: "Cantons Solidaires Autonomes"
subtitle: "Quelles solutions concrètes pour demain&nbsp;?"
handle: "cantons.info"
keywords:
- Autonomie
- Canton
- Entraide
- Partage
- Solidarité
---

<ul>
<li><a href="/a-propos/" class="btn btn-grey">Page de présentation</a></li>
<li><a href="/aide-discord/" class="btn btn-grey">Page d'aide Discord</a></li>
<li><a href="/cantons/" class="btn btn-grey">Liste des Cantons</a></li>
<!--<li><a href="/carte/">Carte interactive</a></li>-->
<li><a href="https://colibris-wiki.org/cantons" target="_blank" class="btn btn-green">Bibliothèque YesWiki</a></li>
</ul>
