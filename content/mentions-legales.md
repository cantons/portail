---
title: "Mentions légales"
toc: true
---

## Hébergement

Le portail est un site statique servi par [Framagit](https://framagit.org), un outil géré par l'association [Framasoft](https://framasoft.org) et hébergé par la société [Hetzner Online GmbH](https://www.hetzner.com).

La [carte interactive](/carte/) basée sur [GoGoCarto](https://gogocarto.fr) est hébergée par la même société.

> **Hetzner Online GmbH**<br>
> Industriestr. 25<br>
> 91710 Gunzenhausen<br>
> Germany

> Tél. : +49 (0)9831 505-0

Le domaine, les DNS du domaine et les adresses électroniques sont hébergés par la société [Gandi SAS](https://www.gandi.net).

> **Gandi SAS**<br>
> 63-65 boulevard Masséna<br>
> 75013 Paris<br>
> France

> Tél. : +33 (0) 1 70 37 76 61

## Cookies

Ce site n'utilise aucun cookie.

## Données personnelles

Ce site n'exploite aucune donnée personnelle, les informations collectées sont celles strictement nécessaires à la création d'une page à partir d'un point sur la carte interactive, à savoir :
- une adresse électronique de préférence impersonnelle,
- et une localisation approximative sous forme de coordonnées.

Les éléments sont publiés sous la responsabilité des utilisateurs et permettent de générer automatiquement la [liste des Cantons](/cantons/).
