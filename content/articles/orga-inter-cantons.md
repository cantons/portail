---
title: "Idées et organisation inter-cantons"
subtitle: "Chaque canton est différent, mais gagnera à se relier aux autres cantons. Séance de partage d'idées et d'expériences."
aliases:
- /compte-rendus/2021-04-28-orga-inter-cantons
date: 2021-04-28T20:00:00+0100
description: |
  Discussions: Idées et organisation inter-cantons. Chaque canton est différent, mais gagnera à se relier aux autres cantons. Séance de partage d'idées et d'expériences.
keywords:
- Compte-rendu
- Inter-cantons
categories:
- Compte-rendu
- Inter-cantons
toc: true
---

## C'est quoi la coordination inter-cantons ?

Listons ce dont on a besoin pour se coordonner entre les cantons au niveau national et international

## Partages de ressources
- affiches, images, flyers
- [bibliothèque autonomie](https://colibris-wiki.org/cantons/?Bibliotheque)
- répertorier tous les maraichers
- lister les bonnes adresses recycleries et repair-cafés et hackerspaces, etc

## Compétences
mutualisation des competences, réseaux inter-cantons par domaine d'activité
- détection de competences spécifiques par canton, répertoriation
- canal #compétence, mais tout le monde partage ses competences personnelles
- du coup peut-etre il faudrait un autre canal pour les cantons
- besoin de classification des competences pour faciliter la lisibilité
- format de la liste : tableau, [carte](https://cantons.info/carte/), papier...
- Il faut des fichiers partagés, au moins un par thème : enseignement, communication, économie...

## Actions inter-cantons
- évènements
- achats groupés
- achat en gros pour tout les produits fait maison 
- centrale d'achat, achat groupés, https://rqdt.org/
- lister les fournisseurs, les procedes, les partenaires de chaque canton
- contact avec les organismes locaux comme par exemple les ecuries qui ont plein de crotin pour la permaculture
- mise en fonctionnement des sources, fontaine avec lavoir, comment faire: l'eau est un bien inter-territoire
- faire un flyer generique

## Communauté en ligne
Animation en ligne, discord, etc
- peut-etre un vocal accueil pour aider les gens a regler leurs problemes de micro
- Gestion des noms de salon (trop de salons).
- trouver plus de volontaires pour la moderation du discord
- rédaction de la charte, de la raison d'être, d'articles
- partenaires/amis, thématiques générales - création d'un rôle Amis ?

## Information inter-canton
nouvelles des cantons, partager un point pour que tous les cantons sachent ce que les autres font, peut-etre tous les mois
- [articles](https://cantons.info/articles/) sur le site
- liste de diffusion mail, par canton, inter-cantons ?
- lettre d'information par canton et peut-etre une lettre inter-canton qui regroupe les news des cantons
- retours d'experience sur ce qui a marché et ce qui a foiré
- format papier imprimable pour diffusion locale? A décider pour chaque canton

## Marché et troc inter-canton
- recycleries (se faire passer les trucs à recycler)
- repair café (se faire passer les trucs à reparer)
- mix entre marché avec echanges monétaires et systeme de troc
- note sur les magasins producteurs qui s'ouvrent un peu partout en ce moment, ils sont vachement chers, y'a besoin d'un truc plus instinctif plus naturel et moins lourd
- c'est mieux la vente directe, lien producteur consommateur sans local ni loyer, ni charges compliquées
- Olivier est volontaire pour créer un groupe inter-canton sur l'échange de graines, semences, plants de légumes bio reproductibles
