---
title: "Création du portail"
subtitle: "Cantons Solidaires Autonomes"
date: 2021-03-26T19:00:00+0100
description: |
  Un dimanche soir de mars 2021, une discussion sur Discord a eu lieu à propos des besoins et possibilités de présence numérique des Cantons Solidaires Autonomes.
  Et depuis, ça frétille et ça bouillonne, un nouveau canton existe : il est numérique.
keywords:
- Canton Numérique
- Portail
categories:
- Canton Numérique
- Portail
toc: true
author: mose
---


Un dimanche soir de mars 2021, une discussion sur Discord a eu lieu à propos des besoins et possibilités de présence numérique des Cantons Solidaires Autonomes. Et depuis, ça frétille et ça bouillonne, un nouveau canton existe : il est numérique.

## Sortir des GAFAM

Il faut bien reconnaître que jusque la chacun se débrouille avec les moyens du bord. Chaque canton étant *autonome* (par définition), la méthode la plus simple, la plus rapide et la moins coûteuse a prévalu. C'est-à-dire un groupe Facebook.

Alors, bien sûr, dépendre des géants de l'internet comme plateforme de collaboration et de partage, ce n'est pas forcement le plus top top. Au cours de ces deux dernières années, il est arrivé plusieurs fois que des volontaires proposent de faire un Site web dédié, avec un nom de domaine et tout le tralala. Mais cela n'a jamais abouti à rien jusqu'ici.

## Un site portail

Alors dans le même esprit de choisir la méthode la plus simple, la plus rapide et la moins coûteuse, une proposition a été faite dans un premier temps de construire un site web statique hébergé et généré sur Framagit, un service gratuit fourni par [Framasoft][1]. Bon alors évidemment, c'est la moins coûteuse pour tout le monde, mais ça n'est simple et rapide que pour des gens qui s'y connaissent un peu dans le monde des technologies de l'internet.

Par chance, il y en a dans le rang des Cantons, des gens qui ont ces compétences. La proposition a été faite par `95 LEI` et `mose tw` a trouvé que c'était une bonne idée. Il n'en fallait pas moins pour que ça se fasse.

Le lendemain `mose tw` faisait une donation du cout du nom de domaine `cantons.info` chez [Gandi][2], et en déléguait la gestion à deux autres techos (appelons-les comme ca) `95 LEI`, `mose tw` et `74 mckmonster` et à `78/50 Annie`.

Illico `95 LEI` a ouvert l'espace [Framagit][3] pour recueillir le code source du portail initial, en déléguant cette fois aussi la gestion de l'espace aux mêmes personne que pour le domaine.

Du coup a commencé un travail de collecte d'information sur les cantons que `75 chaami` a aidé à organiser en utilisant des pad divers sur [CryptPad][4].

## Une carte des cantons

Puisque la force des cantons est territoriale, disposer d'une carte était judicieux, du coup `mose tw` a créé un compte [cantons][5] sur [GoGoCarto][6] et en a également partagé l'administration avec les mêmes que pour le site portail. Du coup un travail est en cours pour ajouter les cantons sur la carte, qui a été inclue dans le site portail pour faciliter les choses.

Chacun peut ajouter son canton sans même créer de compte nulle part, et la modération validera l'ajout dès que possible.

## Un compte Mobilizon

Dans cet élan `74 mckmonster` s'est dit que ça serait chouette aussi d'avoir un canal d'annonce et de diffusion des évènements organisés autour et par les cantons, donc il a fait un compte [Mobilizon][7] et en a partagé l'accès à ceux qui étaient actifs à ce moment-la.

Et maintenant il est en train de bricoler du code pour pouvoir afficher les évènements sur le site portail.

L'avantage de Mobilizon c'est de faire partie du [Fediverse][10], du coup on peut s'abonner aux évènements par le biais de [Mastodon][8] ou [Pleroma][9] (qui sont des alternatives citoyennes à Twitter).

## Une chaine sur PeerTube

Parce qu'il n'aime pas YouTube, `mose tw` a utilisé son compte sur le PeerTube des crapauds fous pour monter une chaîne ou héberger les vidéos des Cantons Solidaires Autonomes. Mais pas seulement.

PeerTube est un trésor de contenus libre et alternatifs, mais il est parfois difficile d'y trouver ce que l'on cherche. Dans la [chaîne PeerTube][11] le but est donc de constituer des [listes de lecture][12] (playlists) qui regroupent toutes les vidéos utiles sur les sujets qui nous intéressent liés à la cantonitude, à l'autonomisation et au solidarisme.

N'hésitez pas à suggérer de nouveaux ajouts!

## Ca bouge dans Discord

Tout ça s'est passé publiquement sur le canal `#action-numérique` sur le Discord des cantons. Et beaucoup de *techos des cantons* ont rejoint cette initiative, en attendant de trouver le moment opportun pour ajouter leur pierre à l'édifice.

Il y a bien sûr de nombreux besoins à satisfaire avant de pouvoir totalement proposer aux cantons de s'évader de Facebook, mais il faut bien que ça commence quelque part.

Donc, si jamais vous êtes un peu à l'aise avec les technologies de l'internet, n'hésitez pas à venir faire un coucou dans ce canal, qui sait peut-être qu'il y aura une évidence de rôle que vous pourriez y jouer pour le bénéfice de tous.

## A suivre

Alors, attention tout de même, le but des cantons c'est de tisser des liens humains réels sur le terrain, ce canton numérique ne fera que fournir des ressources utiles, mais ça n'est pas du tout un but en soi. Juste un outil.

On vous tiendra au courant des évolutions de ce canton numérique au fur et à mesure, viendez nous aider si le coeur vous en dit (et que vous êtes dégourdis en technologie) !


[1]: https://framasoft.org
[2]: https://gandi.net/fr
[3]: https://framagit.org/cantons
[4]: https://cryptpad.fr
[5]: https://cantons.gogocarto.fr/annuaire#/carte/@46.53,-0.66,6z?cat=all
[6]: https://gogocarto.fr/projects
[7]: https://mobilizon.fr/@cantons
[8]: https://fr.wikipedia.org/wiki/Mastodon_%28r%C3%A9seau_social%29
[9]: https://fr.wikipedia.org/wiki/Pleroma
[10]: https://serveur410.com/le-fediverse-cest-quoi-et-comment-lutiliser/
[11]: https://tube.crapaud-fou.org/video-channels/cantons
[12]: https://tube.crapaud-fou.org/video-channels/cantons/video-playlists
