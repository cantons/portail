---
title: "Aide Discord"
subtitle: "Cantons Solidaires Autonomes"
url: /aide-discord
aliases:
- /articles/aide-discord
- /pages/aide-discord
description: ""
keywords:
- Tuto
- Discord
categories:
- Tuto
- Discord
taxo: categories
toc: true
---

## Bienvenue

Une fois votre compte utilisateur créé, vous pouvez rejoindre le serveur Discord des Cantons en cliquant sur le lien ci-dessous :

<!-- {{< content "content/a-propos/info.md" >}} -->
<br>
<center>
{{< icon slug="Discord" url="https://discord.gg/K2SVjfJ" class="discord" >}}
<br>
<a href="https://discord.gg/K2SVjfJ">https://discord.gg/K2SVjfJ</a>
</center>

## Serveur

### Règles à lire

{{< content "content/a-propos/rules.md" >}}

Les données personnelles sensibles telles qu'une adresse (postale ou électronique), un numéro de téléphone ou un document important ne devraient pas être divulguées dans un salon Discord, ni sur internet d'une manière générale.

Vous pouvez lire la charte d'utilisation de la communauté Discord en suivant ce lien : https://discord.com/guidelines.

### Salons

Les **nouveaux arrivants** peuvent consulter les salons `#règles-à-lire`, `#guide-débutant`, `#qui-sont-les-cantons` et le salon écrit `#vocal-accueil-texte` qui accompagne le **Vocal accueil**.

Découvrez l'ensemble des salons et des catégories une fois un rôle attribué :

- **Général** : calendrier des évènements, discussions à propos des Cantons, organisation des réunions et de l'entraide **Inter-Cantons**.
- **Vocaux** : salons vocaux et leurs salons écrits correspondants.
- **Solidarité** : échanges et informations solidaires.
- **Autonomie** : partage de connaissances autour des thématiques générales de l'autonomie pour construire une [bibliothèque](https://colibris-wiki.org/cantons/?Bibliotheque).
- **Fermes, terrains et éco-lieux** : recherche, construction et ateliers.
- **Catégories géographiques** : départements et cantons groupés par région ou pays pour l'international : 
chaque **région** dispose d'un salon texte et d'un salon vocal pour échanger,
chaque **département** a un salon pour la mise en relation entre les Cantons,
et chaque **Canton** a un salon dédié pour partager, organiser des ateliers, réunions ou autre.
- **Le Coin des Copains** : les liens, publications et informations des amis de route.

<!--
Exemple avec le Canton de Rambouillet (78) :

> **`78 <Pseudo>`**
>
> **Rôle régional** : `@Île-de-France`<br>
> **Rôle départemental** : `@78 Yvelines`
>
> **Salon régional** : `#vocal-île-de-france-texte` + salon vocal<br>
> **Salon départemental** : `#78-yvelines`<br>
> **Salon cantonal** : `#78-canton-de-rambouillet`

Exemples avec plusieurs numéros de département ou codes pays :

> **`78/50 <Pseudo>`**
>
> **Rôles** : `@Île-de-France` `@Normandie` `@78 Yvelines` `@50 Manche`<br>
> **Salons** : `#78-yvelines` `#50-manche`

> **`14/BE <Pseudo>`**
>
> **Rôles** : `@Normandie` `@International` `@14 Calvados` `@Belgique`<br>
> **Salons** : `#14-calvados` `#belgique`
-->

### Rôles

- **Rôles géographiques** : région et département, pays pour l'international, ou voyageur ; débloque l'accès au reste du serveur.
- **Aide** : participe à l'accueil des nouveaux arrivants en les guidant,
 assigne les rôles, gère les salons et organise le serveur des Cantons.
- **Concierge** : assure le bon fonctionnement du serveur.

## Tutos

### Astuces

Quelques trucs et astuces :

- [Guide de Discord pour débutants](https://support.discord.com/hc/fr/articles/360045138571-Guide-de-Discord-pour-d%C3%A9butants) :
  - Un **appui long ou clic droit** sur un message, un salon ou une catégorie permet d'accéder aux **actions et paramètres** correspondants.
  - **Mention** : écrire "@" puis une partie du nom, *sans espace*.
    - **Sélectionner** un utilisateur ou un rôle à notifier dans la liste.
    - Les rôles se trouvent toujours **à la fin de la liste** des suggestions.
    - Répondre à un message en le citant peut notifier son auteur.
  - **Raccourci vers un salon** du même serveur : écrire "#" puis le nom.
- [Surnoms de serveurs](https://support.discord.com/hc/fr/articles/219070107-Surnoms-de-serveurs) :
  - Le **numéro de département/code pays** peut être indiqué avant le nom, séparé par un espace.
  - Chaque utilisateur est libre de changer son nom d'affichage par serveur.
- [Catégories de salons](https://support.discord.com/hc/fr/articles/115001580171-Cat%C3%A9gories-de-salons-la-Base) :
  - **Réduire une catégorie**  avec un appui ou un clic simple sur son nom, pour de cacher les salons déjà lus et faciliter la navigation.
  - **Mettre en sourdine** depuis les paramètres de notification afin de limiter les mentions par serveur, catégorie ou salon.
<!-- - [Mettre en sourdine et désactiver les notifications pour des salons spécifiques](https://support.discord.com/hc/fr/articles/209791877-Comment-mettre-en-sourdine-et-d%C3%A9sactiver-les-notifications-pour-des-salons-sp%C3%A9cifiques-) (PC). -->
- [Faire une recherche](https://support.discord.com/hc/fr/articles/115000468588-Faire-une-recherche) : trouver un message et filtrer avec des mots-clés.
- [Configurer le son sur Discord](https://www.youtube.com/watch?v=G0BHj8gGPIQ) avec Windows (vidéo YouTube).

### Accueil

Tout le monde peut souhaiter la bienvenue et est invité à guider les nouveaux arrivants sur le serveur :

1. Un utilisateur rejoint le salon `#nouveaux-arrivants` :
  - accueillir le nouvel arrivant pour **présenter les Cantons** et le guider sur le serveur, éventuellement vocalement,
  - **demander sa zone géographique** afin de déterminer les rôles correspondants et de le mettre en relation avec ses voisins,
  - si la réponse se fait attendre, considérez une relance ou peut-être un message privé, mais pas tous à la fois !
2. L'utilisateur a indiqué sa localisation :
  - **signifier l'accueil en cours** en ajoutant une réaction sur le message d'arrivée ou la réponse du nouvel arrivant (appui long ou clic droit),
  - **proposer d'indiquer** le [numéro de département](https://fr.wikipedia.org/wiki/Liste_des_d%C3%A9partements_fran%C3%A7ais) ou [code pays](https://fr.wikipedia.org/wiki/ISO_3166-1) suivi d'un espace avant son pseudonyme,
  - **assigner les rôles** des pays, région(s) et département(s) correspondants.
3. L'utilisateur peut désormais voir l'ensemble du serveur :
  - si besoin, créer le salon du département dans la catégorie de la région,
  - **mentionner l'utilisateur** et le rôle correspondant ("@" + département ou pays) dans le **salon du département**,
  - mentionner l'utilisateur dans le **salon du Canton** si il existe déjà, ou laisser ses voisins le guider.

---

Nouveau Canton :

  - Créer le **salon du Canton** dans la catégorie correspondante.
  - **Mentionner** les utilisateurs concernés, ou le rôle du département/pays.
  - Ajouter le Canton sur la [carte interactive](/carte/) : https://carte.cantons.info/elements/add.

## Autres liens

- [Présentation](/a-propos/) des Cantons
- [Centre d'aide](https://support.discord.com/hc/fr) officiel de Discord
<!-- - Le site web du [Discord du Canard Réfractaire](https://discord.lecanardrefractaire.org/) -->
