---
title: "Création d'évènement"
subtitle: "Cantons Solidaires Autonomes"
date: 2021-03-29T10:00:00+0100
keywords:
- Canton Numérique
- Portail
- Tuto
- Evènement
categories:
- Canton Numérique
- Portail
- Tuto
- Evènement
toc: true
author: mckmonster
---

## Pré-requis

Tout d'abord, il vous faut un compte sur [Mobilizon](https://mobilizon.fr).
Et ce compte doit être modérateur sur le groupe [Cantons Solidaires Autonomes](https://mobilizon.fr/@cantons).

Pour ça, il faut cliquer sur le bouton rejoindre sur le groupe.

![Rejoindre le groupe](/img/create-event-1.png)

Puis demandez à un admin du groupe sur le [canal #action-numérique du discord](https://discord.com/channels/648256888558977024/823300985321422848) de vous donner les droits modérateur.

Lorsque vous êtes modérateur, vous pouvez créer un évènement.

## Créer un évènement

Sur la page principal du site [Mobilizon](https://mobilizon.fr), il suffit de cliquer sur le bouton "créer".
Voici les étapes :

- Mettre une image pour votre évènement.
- Mettre un titre
- Ajouter des tags (mettre le nom du canton et le numéro du département)
  ![Image, titre et tags](/img/create-event-2.png)
- Mettre une date de début et de fin
  ![Date de début et de fin](/img/create-event-3.png)
  Si vous ne savez pas quand ça commence ou quand ça finit, vous pouvez ne pas l'afficher en cliquant sur "Paramètre de date"
  ![Paramètres de date et d'heure](/img/create-event-4.png)
- Si c'est un lieu physique, vous pouvez mettre une adresse
- Rajoutez une description pour expliquer à quoi correspond votre évènement
- Si c'est en ligne, vous pouvez mettre le lien dans "Site web/URL"
- **Mettre l'organisateur "Cantons Solidaires Autonomes"** ainsi que ceux de la liste qui organise
  ![Choisir un profil ou groupe](/img/create-event-5.png)
- Laisser l'évènement en public
- Pour la participation et la modération à vous de voir
- Pour le status, ça dépend de l'évènement (pensez à l'annuler, si c'est le cas)
- Valider la création de l'évènement

## Félicitation

Ca y est votre évènement est visible sur le site [Mobilizon](https://mobilizon.fr) et sur le site [Cantons Solidaires Autonomes](https://cantons.info/evenements/).

Vous pouvez partager le lien de l'évènement sur les différents réseaux sociaux et dans le canal [#calendrier-réunion](https://discord.com/channels/648256888558977024/775641168293527613)

![Partager l'évènement](/img/create-event-6.png)
