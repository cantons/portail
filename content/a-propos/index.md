---
title: Présentation
author: des Cantons Solidaires Autonomes
description: "L'objectif des Cantons est de réunir les individus souhaitant retrouver leur autonomie et revenir sur les vraies valeurs humaines, telles que la solidarité, l'entraide, l'échange, le partage d'idées, de travaux et de compétences."
aliases:
- /presentation/
resources:
- name: featured
  src: cantons-solidaires-autonomes.png
  title: Logo
toc: true
menu:
  main:
    identifier: about
    name: "+ d'info"
    title: "Plus d'informations"
    url: "/a-propos/"
    weight: 6
---

Les **Cantons Solidaires Autonomes** ont vocation à permettre la naissance de nouveaux modes de vie collectifs.
Seulement, le modèle que nous avons actuellement ne nous convient plus. Une transformation est nécessaire, avec de nouvelles valeurs.

## Qu'est-ce qu'un canton ?

Un [canton français](https://fr.wikipedia.org/wiki/Canton_fran%C3%A7ais) est une limite géographique plus petite qu'un département, qui regroupe en moyenne une dizaine de communes.
C'est une zone arbitraire qui permet de se situer mutuellement afin de former des groupes locaux.

{{< figure "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Frankreich_Kantone_2019.png/857px-Frankreich_Kantone_2019.png" "Carte des cantons en France métropolitaine" "Carte des cantons en France métropolitaine (<a href=\"https://commons.wikimedia.org/wiki/File:Frankreich_Kantone_2019.png\" target=\"_blank\">Wikimedia Commons</a>)" >}}

Les **Cantons Solidaires Autonomes** existent aussi en France d'outre-mer et à l'international, principalement dans les régions francophones.

## Ce que nous ne voulons plus

Etre dépendant d'une structure pyramidale, dans laquelle notre conduite, notre façon de vivre ou de penser, sont fortement influencées et aiguillées, pour répondre à une « norme », ne nous permettant pas de nous réaliser en tant qu'êtres.

Etre totalement soumis à un système, principalement basé sur des valeurs financières et monétaires, que nous avons tous alimenté et qui ne nous convient plus.

Nous vivons actuellement, un passage important, un tournant dans l'Histoire de l'humanité.

Qu'en feront les peuples après cette crise mondiale (sanitaire, économique, sociale, morale...) ?

Etes-vous en train de vous préparer ? Etes-vous prêts à changer ?

## Ce que nous souhaitons

A travers les **Cantons Solidaires Autonomes**, nous souhaitons donner aux personnes les moyens de se réunir pour retrouver leur autonomie. Celle-ci est compliquée individuellement, nous proposons donc l'**autonomie participative** collective/collaborative, en partageant le savoir-faire de chacun dans l'entraide, la solidarité et surtout dans un retour aux valeurs humaines.

Apprendre à vivre avec le minimum d'argent. Les **Cantons Solidaires Autonomes** vous proposent de repartir sur des principes humanitaires plutôt que monétaires.
Nous avons la possibilité de faire autrement, nos ressources naturelles (individuelles et collectives) sont grandes : recyclage, récupération, réparation plutôt que surconsommation, mise en place de troc, d'échange…
Et pourquoi pas envisager l'utilisation des monnaies locales ?

Revenir à l'essentiel et préserver nos ressources naturelles, de façon à ce que nos enfants apprennent à vivre différemment. Dans le respect du Vivant sous toutes ses formes (des règnes humain, animal, végétal, minéral…).
Nous pourrons par la suite, créer des groupes de travail par thèmes (protection de la nature, des animaux, économie, enseignement, santé, justice…). Unir nos forces pour vivre mieux.

Organiser des réunions par canton, sur le terrain, pour échanger nos idées et avancer ensemble dans l'autonomie.
Nous vous proposons de créer vos propres groupes par canton, pour fédérer tous les citoyens et travailler ensemble sur des projets communs.

Chaque canton est **autonome, libre de fonctionnement**, mais relié aux autres, par divers moyens, de façon à nous unir, partager nos idées, travaux et compétences.
Nous sommes dans l'horizontalité, chacun et chacune d'entre nous pouvant donner ses idées et s'exprimer.

## Comment y parvenir ?

Merci à vous toutes et tous de respecter ces principes :

### Entre nous

Créer un environnement bienveillant et chaleureux est de notre responsabilité à tous. Traitons-nous les uns les autres, avec **respect**. Il est normal d'avoir des débats constructifs, mais il est essentiel de rester **aimable et courtois**.

Faites en sorte que tout le monde se sente en **sécurité**. Le harcèlement sous toutes ses formes, les incitations à la haine, les discriminations (relatives à la couleur de peau, la religion, la culture…), ne sont pas tolérés.

Faire partie de ce groupe implique une **confiance mutuelle** et le respect des thématiques des salons. Des discussions animées et authentiques font tout l'intérêt des groupes, mais elles peuvent aussi contenir des informations sensibles et personnelles. Merci d'observer une **discrétion** sans faille pour maintenir la **confidentialité** des échanges et partages.

Chasser les pensées absolues et exclusives. Nous sommes en train de construire un nouvel écosystème collectif. Dans cette dynamique, il est nécessaire de garder une grande ouverture d'esprit et d'éviter toute forme de stigmatisation.

### Respectez l'environnement

L'environnement et le patrimoine commun (ressources et milieux naturels, espèces animales et végétales, diversité et équilibre biologiques…) doivent être respectés. Laissons les lieux de nos réunions et de nos rencontres propres.

L'environnement de ce groupe, doit rester dans de bonnes énergies. Les liens d'informations sur l'actualité mondiale (décisions politiques, crise sanitaire, manifestations…) sont facilement accessibles sur le net. Merci de ne pas polluer les canaux de communication des groupes avec ce genre de partage souvent anxiogène et générateur d'énergies basses. De plus, en continuant à parler des sujets anxiogène, nous leur donnons de l'importance. Si nous souhaitons donner naissance à un nouveau système, alimentons nos réunions et échanges avec tous les sujets que nous souhaitons voir émerger.

### Ressources

Offrez plus que vous ne prenez au sein de ce groupe.

Chacun possède des compétences, des talents, des facultés. Il est souhaitable de les faire connaître, pour favoriser les échanges de services, les partages de savoir-faire. Cependant, les pratiques d'auto-promotion, de publicité, de captation de clientèle ne sont pas souhaitées. Plutôt que la promotion personnelle, nous cherchons à favoriser la promotion des qualités de tous. Il est donc souhaitable de privilégier des annuaires de compétences plutôt que la publicité personnelle.

Partagez vos expériences en mettant l'accent sur les solutions trouvées, pour améliorer votre quotidien.

## Retrouvez vos voisins

<br>
<br>
<center>
{{< icon slug="Discord" url="/aide-discord/" class="discord" target="_self" >}}
<br>
La page <a href="/aide-discord/">Aide Discord</a> décrit le fonctionnement du serveur.
</center>

<br>
<br>
<center>
{{< icon slug="Carte" url="/carte/" target="_self" >}}
<br>
La <a href="/carte/">carte interactive</a> répertorie les Cantons qui le souhaitent.
</center>

## Les valeurs que nous partageons

#amitié<br>
#solidarité<br>
#autonomie<br>
#respect<br>
#communication<br>
#environnement<br>
#équitabilité

## En route vers l'autonomie !

Vidéo de novembre 2020 :

{{< peertube "tube.crapaud-fou.org" "338c3ae4-780f-47e6-9311-bbd6075692f8" >}}

## Les Copains

{{< partners >}}
