Les **Cantons Solidaires Autonomes** vous proposent de repartir sur l'humanitaire plutôt que le monétaire.
De préserver nos ressources naturelles, de façon à ce que nos enfants réapprennent à vivre différemment. 

Nous proposons de créer vos propres groupes par canton pour unir tous les citoyens, et de travailler sur des projets ensemble de façon à ce que le peuple récupère le pouvoir. 
Les cantons seront autonomes mais reliés tous ensemble sur une plate-forme neutre de façon à nous unir, entrer dans l'autonomie, partager nos idées, travaux et compétences. 

Le partage, la solidarité, éventuellement faire du troc, échanger des ateliers recettes entre les différents cantons.
Unir nos forces pour vivre mieux.

Nous serons dans l'horizontalité et chacun(es) d'entre nous pourra donner ses idées et s'exprimer.
Nous imaginons de créer des groupes de travail par thèmes (protection de la nature, des animaux, de l'économie, de l'enseignement, de la santé, de la justice etc).

Nous pourrons organiser des réunions par canton sur le terrain pour échanger nos idées et avancer ensemble dans l'autonomie et utiliser les monnaies locales. 
