Merci à vous toutes et tous de respecter ces règles :

- **Soyez aimable et courtois**<br>
Créer un environnement chaleureux, c'est de notre responsabilité à tous. Traitons tout le monde avec respect. Il est normal d'avoir des débats constructifs, mais il est essentiel de rester aimable. Toute insulte répétée mènera à un bannissement.

- **Pas d'incitation à la haine, ni de harcèlement**<br>
Faites en sorte que tout le monde se sente en sécurité. Le harcèlement sous toutes ses formes est interdit et les commentaires dégradants qui portent sur la couleur de peau, la religion, la culture.

- **Pas de promotions, ni de contenu indésirable**<br>
Offrez plus que vous ne prenez au sein de ce groupe. L'auto-promotion, les contenus indésirables et les liens non pertinents ne sont pas autorisés et seront supprimés. Est indésirable tout partage d'information qui se focalise sur des problèmes plutôt que des solutions autour de la solidarité et de l'autonomie.

- **Respectez l'environnement et la confidentialité de tous**<br>
Faire partie de ce groupe implique une confiance mutuelle et de respecter les thématiques des salons. Des discussions animées et authentiques font tout l'intérêt des groupes, mais elles peuvent aussi contenir des informations sensibles et personnelles.
