Les **Cantons Solidaires Autonomes** ont pour objectif de réunir les personnes qui souhaitent retrouver leur autonomie et leur liberté.

L'autonomie est compliquée individuellement, nous proposons donc l'**autonomie participative** en partageant le savoir-faire de chacun dans l'entraide, la solidarité et surtout dans un retour aux vraies valeurs humaines et non plus monétaires.

Réapprendre à vivre avec le minimum d'argent de façon à ce que les gens comprennent que, même sans argent, nous sommes puissants.
Cela permet en même temps de ne plus alimenter le système actuel que nous combattons pour reconstruire un système humain.

Nous vivons une page de l'histoire mondiale, qu'en feront les peuples après ce confinement et tout ce chômage qui nous attend ?
Etes-vous en train de vous préparer ?

L'heure est venue de retrouver notre humanité, notre autonomie, notre solidarité et notre dû. 
