---
type: pages
layout: events
title: "Evènements"
subtitle: |
  Comment créer un évènement Mobilizon ?
  Consultez le tutoriel de <a href="/2021/03/29/creation-devenement/" target="_blank">création d'évènement</a> dans les articles.
aliases:
- /events
description: "L'objectif des cantons est de réunir les individus souhaitant retrouver leur autonomie et revenir sur les vraies valeurs humaines, telles que la solidarité, l'entraide, l'échange, le partage d'idées, de travaux et de compétences."
keywords:
- Calendrier
- Evènements
- Mobilizon
menu:
  main:
    identifier: events
    name: "Evènements"
    title: "Les évènements des cantons"
    url: "/evenements/"
    weight: 5
---

Abonnez-vous à <a href="https://mobilizon.fr/@cantons" target="_blank">https://mobilizon.fr/@cantons</a> pour suivre la publication des évènements !
