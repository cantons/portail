---
type: pages
layout: canton
title: Canton de Houdan (78)
url: /cantons/78550/canton-de-houdan-(78)
aliases: []
date: "2021-03-12T00:00:00+01:00"
description: ""
resources: []
keywords:
- Île-de-France
- Yvelines
- Houdan
- "78"
tags: []
regions:
- Île-de-France
- Yvelines
- Houdan
- "78"
competences: []
participants: 6
social: []
friends: []
toc: true
lat: 48.7912
lon: 1.60192
---

## Présentation

Mettre en place une synergie locale de résilience alimentaire, d'entraide et de partage autour de jardins participatifs, apprendre l'autonomie matérielle et réapprendre le en lien social.

## Adresse

78 Houdan
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Canton-de-Houdan-(78)/K/" id="map" >}}
