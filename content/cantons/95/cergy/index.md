---
type: pages
layout: canton
title: Canton de Cergy
url: /cantons/95/cergy
aliases:
- /cantons/95000/canton-de-cergy
date: "2021-03-14T00:00:00+01:00"
description: L’objectif des Cantons est de réunir les individus souhaitant retrouver
  leur autonomie et revenir sur les vraies valeurs humaines, telles que la solidarité,
  l’entraide, l’échange, le partage d’idées, de travaux et de compétences.
resources:
- name: featured
  src: image.jpg
keywords:
- France
- Île-de-France
- Val-d'Oise
- "95"
- Cergy
- Informatique
- Peinture
- Photographie
tags: []
regions:
- France
- Île-de-France
- Val-d'Oise
- "95"
- Cergy
competences:
- Informatique
- Peinture
- Photographie
participants: 3
social:
- title: Discord
  url: https://discord.gg/K2SVjfJ
friends: []
toc: true
lat: 49.05269
lon: 2.03886
---

## Présentation

On se retrouvera sur le terrain ou sur [Discord](https://discord.gg/K2SVjfJ) pour échanger nos idées et avancer ensemble dans l’autonomie.

## Compétences

{{< skills >}}

## Adresse

95 Cergy
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Canton-de-Cergy/1/" id="map" >}}

## Discord

{{< social >}}

<center><a href="https://discord.gg/K2SVjfJ" target="_blank">https://discord.gg/K2SVjfJ</a></center>
