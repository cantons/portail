---
type: pages
layout: canton
title: Canton de l'Isle-sur-la-Sorgue
url: /cantons/84250/canton-de-l'isle-sur-la-sorgue
aliases: []
date: "2021-11-20T00:00:00+01:00"
description: Initiative collaborative pour créer un canton autonome dans cette belle
  région de Provence.
resources: []
keywords:
- "84"
- Le Thor
tags: []
regions:
- "84"
- Le Thor
competences: []
participants: 0
social: []
friends: []
toc: true
lat: 43.92732
lon: 4.99481
---

## Adresse

84250 Le Thor
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Canton-de-l'Isle-sur-la-Sorgue/t/" id="map" >}}
