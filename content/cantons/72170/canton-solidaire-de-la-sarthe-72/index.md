---
type: pages
layout: canton
title: Canton solidaire de la Sarthe 72
url: /cantons/72170/canton-solidaire-de-la-sarthe-72
aliases: []
date: "2021-05-21T09:46:56+02:00"
description: ""
resources: []
keywords:
- "72"
- Beaumont-sur-Sarthe
tags: []
regions:
- "72"
- Beaumont-sur-Sarthe
competences: []
participants: 0
social: []
friends: []
toc: true
lat: 48.22647
lon: 0.13282
---
