---
type: pages
layout: canton
title: Cantons de la Martinique
url: /cantons/972/cantons-de-la-martinique
aliases:
- /cantons/cantons-de-la-martinique
date: "2021-01-18T00:00:00+01:00"
description: Depuis sa création les Cantons 972 s'agrandissent, plusieurs réunions
  ont pu déjà avoir lieu principalement sur les communes du Robert et de Trinité.
  L'heure est venue de faire connaissance et d'échanger entre les membres des cantons.
resources:
- name: featured
  src: image.jpg
keywords:
- France
- Outre-mer
- Martinique
- "972"
tags: []
regions:
- France
- Outre-mer
- Martinique
- "972"
competences: []
participants: 0
social:
- title: Telegram
  url: https://t.me/joinchat/2eX4Etc6J-wwYjU5
friends:
- title: Zéro Déchet Martinique
  url: https://www.zd972.com
  src: https://www.zd972.com/themes/zd972/img/logo.png
toc: true
lat: 14.63679
lon: -61.01582
---

## Présentation

Nous vivons une page de l'histoire mondiale, qu'en feront les peuples après ce confinement et tout ce chômage qui nous attend ? Etes-vous en train de vous préparer ? L'heure est venue de retrouver notre humanité, notre autonomie, notre solidarité et notre dû.

Les **Cantons Solidaires Autonomes** vous proposent de repartir sur l'humanitaire plutôt que le monétaire :

- préserver nos ressources naturelles, de façon que nos enfants réapprennent à vivre différemment,

- unir tous les citoyens et travailler sur des projets ensemble de façon à ce que le peuple récupère le pouvoir.

Les cantons seront autonomes, mais reliés tous ensemble sur une plateforme neutre de façon à converger, entrer dans l'autonomie, partager nos idées, travaux et compétences.

Le partage, la solidarité, éventuellement faire du troc, échanger des ateliers recettes entre les différents cantons. Unir nos forces pour vivre mieux. Nous serons dans l'horizontalité et chacun(es) d'entre nous pourra donner ses idées et s'exprimer.

Nous imaginons de créer des groupes de travail par thèmes (protection de la nature, des animaux, de l'économie, de l'enseignement, de la santé, de la justice, etc). Nous pourrons organiser des réunions par canton sur le terrain pour échanger nos idées et avancer ensemble dans l'autonomie et utiliser les monnaies locales.

{{< figure "https://cantons.gogocarto.fr/uploads/cantons/images/elements/martinique.jpg" "Martinique" "Canton Solidaire Autonome de la Martinique (972)" >}}

N'hésitez pas à en parler autour de vous, et si vous connaissez des personnes intéressées dans votre secteur, n'hésitez pas à leur communiquer nos coordonnées, car il peut y avoir un ou plusieurs référents par canton, mais un seul groupe par canton pour éviter la division, et s'il n'est pas référencé, il risque de se retrouver isolé.

Il faut faire un maillage de façon à réussir tous ensemble, car c'est tous ensemble que nous pourrons aller plus loin.

## Projets

Depuis sa création les _Cantons 972_ s'agrandissent, plusieurs réunions ont pu déjà avoir lieu principalement sur les communes du Robert et de Trinité. L'heure est venue de faire connaissance et d'échanger pour pouvoir trouver des terrains à exploiter entre les membres des cantons. De nouveaux membres arrivent et nous comptons organiser une rencontre à l'occasion d'un pique-nique pour faire connaissance et grossir le réseau d'entraide.

A ce jour nous sommes dans l'attente d'un terrain (travaux de terrassement) en vue d'un village autonome, disponible dans les prochains mois.

Atelier coup de main chez l'un de nos membres pour déblayer gravas, égaliser le terrain et le préparer à la permaculture... Projet d'installer un container qui pourrait accueillir des ateliers divers.

Nous avons un partenariat amical avec l'association [Zéro Déchet Martinique](https://www.zd972.com) qui proposera régulièrement des ateliers divers : calebasses, low-tech, construction de palettes, etc.

{{< friends >}}

Plusieurs collectifs travaillent dans la même dynamique que les cantons et nous partageons leurs informations. Il s'agit de jardins partagés, d'adresses de producteurs et de produits bio dans le cadre des circuits courts, de groupes de troc, de dons et d'échange sans argent...

Nous projetons une grande rencontre, le premier week-end officiel des Cantons de la Martinique, dont la date reste à déterminer !

Rejoignez le groupe [Telegram](https://t.me/joinchat/2eX4Etc6J-wwYjU5) _Canton Solidaire Autonome 972_ uniquement pour les membres demeurant en Martinique !

## Adresse

Martinique 972
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Cantons-de-la-Martinique/r/" id="map" >}}

## Telegram

{{< social >}}

<center><a href="https://t.me/joinchat/2eX4Etc6J-wwYjU5" target="_blank">https://t.me/joinchat/2eX4Etc6J-wwYjU5</a></center>
