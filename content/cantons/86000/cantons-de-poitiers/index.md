---
type: pages
layout: canton
title: Cantons de Poitiers
url: /cantons/86000/cantons-de-poitiers
aliases: []
date: "2021-07-21T13:05:50+02:00"
description: Poitiers et prox.
resources:
- name: featured
  src: image.jpg
keywords:
- "86"
- Poitiers
tags: []
regions:
- "86"
- Poitiers
competences: []
participants: 0
social: []
friends: []
toc: true
lat: 46.58126
lon: 0.34237
---

## Adresse

86000 Poitiers
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Cantons-de-Poitiers/h/" id="map" >}}
