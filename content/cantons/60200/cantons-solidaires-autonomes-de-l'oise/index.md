---
type: pages
layout: canton
title: Cantons Solidaires Autonomes de l'Oise
url: /cantons/60200/cantons-solidaires-autonomes-de-l'oise
aliases: []
date: "2021-08-07T22:21:01+02:00"
description: 'Apprendre à faire produits ménager, cosmétologie. Permaculture, jardinage,
  tout ce qui consterné l autonomie l entre aide et partage.. '
resources: []
keywords:
- Haut de France
- Picardie
- Oise
- Compiègne
- "60"
tags: []
regions:
- Haut de France
- Picardie
- Oise
- Compiègne
- "60"
competences: []
participants: 0
social: []
friends: []
toc: true
lat: 49.41794
lon: 2.82631
---

## Adresse

60200 Compiègne
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Cantons-Solidaires-Autonomes-de-l'Oise/k/" id="map" >}}
