---
type: pages
layout: canton
title: Canton des Alpes Maritimes 06
url: /cantons/06270/canton-des-alpes-maritimes-06
aliases: []
date: "2021-04-08T14:53:59+02:00"
description: ""
resources:
- name: featured
  src: image.gif
keywords:
- "06"
- Villeneuve-Loubet
tags: []
regions:
- "06"
- Villeneuve-Loubet
competences: []
participants: 10
social: []
friends: []
toc: true
lat: 43.63292
lon: 7.13719
---

## Présentation

Bienvenue

L’objectif des cantons est de réunir les individus souhaitant retrouver leur autonomie et revenir sur les vraies valeurs humaines, telles que la solidarité, l’entraide, l’échange, le partage d’idées, de travaux et de compétences.

Nous pourrons nous retrouver sur un jardin participatif et cultiver nos propres légumes et fruits, faire du troc, proposer des barbecues, pique-niquer et animer des ateliers recettes de façon à retrouver le lien social.

Unir nos forces pour vivre mieux.

Nous serons dans l’horizontalité et chacun(es) d’entre nous pourra donner ses idées et s’exprimer.

Il serait bien de pouvoir réunir des personnes qui s’intéressent à la protection de la Nature, la permaculture, aux animaux, à l’enseignement, à la santé, à la justice, à l’économie, etc.

Nous pourrons organiser des réunions sur le terrain ou en salle pour échanger nos idées et avancer ensemble dans l’autonomie et pourquoi pas utiliser la monnaie locale.

## Site web

{{< social >}}

<center><a href="https://cantons.info/" target="_blank">https://cantons.info/</a></center>
