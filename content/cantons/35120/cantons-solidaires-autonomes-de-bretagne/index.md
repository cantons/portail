---
type: pages
layout: canton
title: Cantons Solidaires Autonomes de Bretagne
url: /cantons/35120/cantons-solidaires-autonomes-de-bretagne
aliases: []
date: "2020-03-13T00:00:00+01:00"
description: "France - Bretagne - Côtes d'Armor 22 - Finistère 29 - Ille et Vilaine
  35 - Morbihan 56\r\n"
resources:
- name: featured
  src: image.jpg
keywords:
- "35"
- La Boussac
tags: []
regions:
- "35"
- La Boussac
competences: []
participants: 0
social: []
friends: []
toc: true
lat: 48.51317
lon: -1.66053
---

## Présentation

Bienvenue dans le Canton Solidaire Autonome du 22 & 35,

L'objectif de ce canton (qui est un lieu) et de réunir les personnes souhaitant retrouver leur autonomie et revenir sur les vraies valeurs humaines, telle que la solidarité, l'entraide, l'échange, partager nos idées, travaux et compétences.

Nous pourrons nous retrouver sur un jardin participatif et cultiver nos propres légumes et fruits, faire du troc, proposer des barbecues, pique-nique et animer des ateliers recettes de façon à retrouver le lien social. Unir nos forces pour vivre mieux.

Nous serons dans l'horizontalité et chacun(es) d'entre nous pourra donner ses idées et s'exprimer. Il serait bien de pouvoir réunir des personnes qui s'intéresse à la Protection de la nature, la permaculture, des animaux, l'enseignement, de la santé, de la justice, de l'économie etc.

Nous pourrons organiser des réunions sur le terrain ou en salle pour échanger nos idées et avancer ensemble dans l'autonomie et pourquoi pas utiliser la monnaie locale également.
