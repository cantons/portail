---
type: pages
layout: canton
title: Canton Solidaire Autonome du 83
url: /cantons/canton-solidaire-autonome-du-83
aliases: []
date: "2021-02-21T00:00:00+01:00"
description: ""
resources:
- name: featured
  src: image.jpg
keywords:
- Saint-Raphaël
tags: []
regions:
- Saint-Raphaël
competences: []
participants: 3
social: []
friends: []
toc: true
lat: 43.42553
lon: 6.76942
---

## Présentation

**Canton Solidaire Autonome du 83**

Rejoignez nous pour nous réunir, partager et construire le nouveau monde de demain.

A Bientôt
