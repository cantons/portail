---
type: pages
layout: canton
title: Rambouillet 78
url: /cantons/78/rambouillet
aliases:
- /cantons/78120/rambouillet-78
date: "2020-10-16T00:00:00+02:00"
description: Bienvenue dans le Canton Solidaire Autonome de Rambouillet !
resources:
- name: featured
  src: image.jpg
keywords:
- "78"
- Rambouillet
- Recettes cosmétiques
tags: []
regions:
- "78"
- Rambouillet
competences:
- Recettes cosmétiques
participants: 25
social:
- title: Portail
  url: https://cantons.info/cantons/78/rambouillet/
friends: []
toc: true
lat: 48.63797
lon: 1.82642
---

## Présentation

L'objectif de ce canton (qui est un lieu) et de réunir les personnes souhaitant retrouver leur autonomie et revenir sur les vraies valeurs humaines, telle que  la solidarité, l'entraide, l'échange, partager nos idées, travaux et compétences.

Nous pourrons nous retrouver sur un jardin participatif et cultiver nos propres légumes et fruits, faire du troc, proposer des barbecue, pique nique et animer des ateliers recettes  de façon a retrouver le lien social.

Unir nos forces pour vivre mieux.

Nous serons dans l'horizontalité et chacun(es) d'entre nous pourra donner ses idées et s'exprimer.

Il serait bien de pouvoir réunir des personnes qui s'intéresse à la Protection de la nature, la permaculture, des animaux, l'enseignement, de la santé, de la justice, de l'économie etc.

Nous pourrons organiser des réunions sur le terrain ou en salle pour échanger nos idées et avancer ensemble dans l'autonomie et pourquoi pas utiliser la monnaie locale également.

## Compétences

{{< skills >}}

## Adresse

78 Rambouillet
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Rambouillet-78/2/" id="map" >}}

## Portail

{{< social >}}

<center><a href="https://cantons.info/cantons/78/rambouillet/" target="_blank">https://cantons.info/cantons/78/rambouillet/</a></center>
