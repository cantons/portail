---
type: pages
layout: canton
title: Montluçon
url: /cantons/03100/montlucon
aliases: []
date: "2021-04-05T23:07:27+02:00"
description: ""
resources:
- name: featured
  src: image.jpg
keywords:
- "03"
- Montluçon
tags: []
regions:
- "03"
- Montluçon
competences: []
participants: 0
social: []
friends: []
toc: true
lat: 46.33992
lon: 2.60672
---

## Présentation

Nous vous accueillerons dans la bienveillance et le partage afin de retrouver la solidarité, ce lien fraternel dont nous avons tous besoin.

Remettre l’autonomie sous toutes ses formes au goût du jour ainsi que le don, le troc, l’échange de services et bien d’autres choses…

Vivre avec des besoins humanitaires et non plus monétaires, dans la convivialité.

Développer des jardins partagés participatifs partout dans notre département.

À vos bêches, arrosoirs et avec votre coeur, cantonnières et cantonniers, semons les petites graines pour un avenir meilleur.

Vous avez des connaissances, des savoirs ou juste l’envie d’apprendre et participer à cette belle aventure alors rejoignez nous.

## Adresse

03 Montluçon
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Montlucon/A/" id="map" >}}
