---
type: pages
layout: canton
title: Canton solidaire autonome Gironde 33
url: /cantons/canton-solidaire-autonome-gironde-33
aliases: []
date: "2021-03-30T22:41:31+02:00"
description: ""
resources: []
keywords:
- Bordeaux
tags: []
regions:
- Bordeaux
competences: []
participants: 80
social:
- title: Groupe privé sur Fb https://www.facebook.com/groups/1243157256099650/
  url: https://discord.gg/eZV5uAM7
friends: []
toc: true
lat: 44.84122
lon: -0.58003
---

## Présentation

Nous espérons retrouver notre souveraineté, de vraie valeur humaine, créer des échanges pour contourner le système actuel, et par la suite créer un lieu de vie en auto suffisance.

## Adresse

Bordeaux
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Canton-solidaire-autonome-Gironde-33/8/" id="map" >}}

## Groupe privé sur Fb https://www.facebook.com/groups/1243157256099650/

{{< social >}}

<center><a href="https://discord.gg/eZV5uAM7" target="_blank">https://discord.gg/eZV5uAM7</a></center>
