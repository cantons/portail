---
type: pages
layout: canton
title: Cantons Solidaires Autonomes du Morbihan (56)
url: /cantons/56130/cantons-solidaires-autonomes-du-morbihan-(56)
aliases: []
date: "2021-04-16T12:40:01+02:00"
description: Bretagne, Morbihan, canton de Vannes, Saint-Dolay, à la limite du 35
  et 44
resources:
- name: featured
  src: image.jpg
keywords:
- "56"
- Saint-Dolay
- Permaculture
- jardins partagés
- travaux manuel
- DIY...et plus encore
tags: []
regions:
- "56"
- Saint-Dolay
competences:
- Permaculture
- jardins partagés
- travaux manuel
- DIY...et plus encore
participants: 10
social: []
friends: []
toc: true
lat: 47.54529
lon: -2.15503
---

## Présentation

Retrouver notre autonomie alimentaire, les valeurs humanistes du partage et de la solidarité en utilisant nos ressources pour se passaer de celles du banquier et de l'agroalimentaire!

Unir nos forces pour vivre mieux et peut-être survivre!

Redonner à nos cantons ce visage humain et naturel de la France avec ses forêts, ses cours d'eau libérés de la main de l'homme qui ne pense qu'à détrourner la nature à son profit et redonner vie à ses petits hameaux autonomes... pour pouvoir se passer au maximum de la voiture pour se déplacer... une France de nouveau rurale et citadine de manière équilibrée qui correspond aux besoins de ses habitants.

Projets multiples et en construction....

## Compétences

{{< skills >}}

## Adresse

56130 Saint-Dolay
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Cantons-Solidaires-Autonomes-du-Morbihan-(56)/L/" id="map" >}}
