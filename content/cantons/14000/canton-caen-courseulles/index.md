---
type: pages
layout: canton
title: Canton Caen Courseulles
url: /cantons/14000/canton-caen-courseulles
aliases: []
date: "2021-04-27T22:35:50+02:00"
description: 'Rencontrons nous pour jardiner, faire des ateliers ensemble, monter
  en compétences autonomes '
resources: []
keywords:
- "14"
- Caen
tags: []
regions:
- "14"
- Caen
competences: []
participants: 0
social: []
friends: []
toc: true
lat: 49.181
lon: -0.36989
---
