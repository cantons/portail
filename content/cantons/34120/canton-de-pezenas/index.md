---
type: pages
layout: canton
title: Canton de Pézenas
url: /cantons/34120/canton-de-pezenas
aliases: []
date: "2021-06-02T09:15:41+02:00"
description: ""
resources: []
keywords:
- "34"
- Pézenas
tags: []
regions:
- "34"
- Pézenas
competences: []
participants: 52
social:
- title: Facebook
  url: https://www.facebook.com/groups/664581877675022/
friends: []
toc: true
lat: 43.46117
lon: 3.42283
---

## Adresse

34120 Pézenas
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Canton-de-Pezenas/g/" id="map" >}}

## Facebook

{{< social >}}

<center><a href="https://www.facebook.com/groups/664581877675022/" target="_blank">https://www.facebook.com/groups/664581877675022/</a></center>
