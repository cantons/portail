---
type: pages
layout: canton
title: Montrouge
url: /cantons/92120/montrouge
aliases: []
date: "2021-04-09T19:35:40+02:00"
description: ""
resources:
- name: featured
  src: image.png
keywords:
- "92"
- Montrouge
tags: []
regions:
- "92"
- Montrouge
competences: []
participants: 1
social: []
friends: []
toc: true
lat: 48.81244
lon: 2.31608
---

## Présentation

Objectif : développer l'autonomie alimentaire, énergétique...

Fermes urbaines, espaces verts potagers, arbres fruitiers... développons ici (et dans chaque commune) des débuts d'autonomie alimentaire. Panneaux solaires, géothermie...

## Site web

{{< social >}}

<center><a href="https://www.montrouge.org/" target="_blank">https://www.montrouge.org/</a></center>
