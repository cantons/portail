---
type: pages
layout: canton
title: Yzeure/Moulins/Avermes
url: /cantons/03400/yzeure-moulins-avermes
aliases: []
date: "2021-03-25T00:33:12+01:00"
description: ""
resources:
- name: featured
  src: image.jpg
keywords:
- "03"
- Yzeure
- Moulins
- Avermes
tags: []
regions:
- "03"
- Yzeure
- Moulins
- Avermes
competences: []
participants: 0
social: []
friends: []
toc: true
lat: 46.56765
lon: 3.35397
---

## Présentation

Nous vous accueillerons dans la bienveillance et le partage afin de retrouver la solidarité, ce lien fraternel dont nous avons tous besoin.

Remettre l’autonomie sous toutes ses formes au goût du jour ainsi que le don, le troc, l’échange de services et bien d’autres choses…

Vivre avec des besoins humanitaires et non plus monétaires, dans la convivialité.

Développer des jardins partagés participatifs partout dans notre département.

À vos bêches, arrosoirs et avec votre coeur, cantonnières et cantonniers, semons les petites graines pour un avenir meilleur.

Vous avez des connaissances, des savoirs ou juste l’envie d’apprendre et participer à cette belle aventure alors rejoignez nous.

## Adresse

03400 Yzeure
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Yzeure-Moulins-Avermes/5/" id="map" >}}
