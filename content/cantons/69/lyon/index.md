---
type: pages
layout: canton
title: Canton de Lyon (69)
url: /cantons/69/lyon
aliases:
- /cantons/canton-de-lyon-(69)
date: "2021-04-11T00:00:00+02:00"
description: Apprendre et partager sur l'autonomie (alimentaire, énergétique, financière,
  informationnelle, sanitaire... ) dans le Rhône. sous toutes ces formes, (jardins
  partagés, habitat participatif, village autonome, eco-lieu, eco-village,  troc,
  solidarité...
resources: []
keywords:
- France
- Auvergne
- Rhône-Alpes
- "69"
- Lyon
- Informatique
- Permaculture
- Logistique
- Peinture
- Photographie
- eco-construction
tags: []
regions:
- France
- Auvergne
- Rhône-Alpes
- "69"
- Lyon
competences:
- Informatique
- Permaculture
- Logistique
- Peinture
- Photographie
- eco-construction
participants: 0
social:
- title: Facebook
  url: https://www.facebook.com/Canton-solidaire-autonome-69-101601365414406
friends: []
toc: true
lat: 45.75781
lon: 4.83201
---

## Présentation

L’objectif des cantons est de réunir les individus souhaitant retrouver leur autonomie et revenir sur les vraies valeurs humaines, telles que la solidarité, l’entraide, l’échange, le partage d’idées, de travaux et de compétences.

Nous pourrons nous retrouver sur un jardin participatif et cultiver nos propres légumes et fruits, faire du troc, proposer des barbecues, pique-niquer et animer des ateliers recettes de façon à retrouver le lien social.

Unir nos forces pour vivre mieux.

Nous serons dans l’horizontalité et chacun(es) d’entre nous pourra donner ses idées et s’exprimer.

Il serait bien de pouvoir réunir des personnes qui s’intéressent à la protection de la Nature, la permaculture, aux animaux, à l’enseignement, à la santé, à la justice, à l’économie, etc.

Nous pourrons organiser des réunions sur le terrain ou en salle pour échanger nos idées et avancer ensemble dans l’autonomie et pourquoi pas utiliser la monnaie locale.

## Compétences

{{< skills >}}

## Adresse

Lyon
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Canton-de-Lyon-(69)/M/" id="map" >}}

## Facebook

{{< social >}}

<center><a href="https://www.facebook.com/Canton-solidaire-autonome-69-101601365414406" target="_blank">https://www.facebook.com/Canton-solidaire-autonome-69-101601365414406</a></center>
