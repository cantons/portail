---
type: pages
layout: canton
title: Canton de Lille
url: /cantons/canton-de-lille
aliases: []
date: "2021-05-13T21:47:28+02:00"
description: ""
resources: []
keywords: []
tags: []
regions: []
competences: []
participants: 0
social:
- title: Facebook
  url: https://www.facebook.com/groups/2637789973145014/
friends: []
toc: true
lat: 50.52896
lon: 3.08835
---

## Adresse

59 Lille
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Canton-de-Lille/T/" id="map" >}}

## Facebook

{{< social >}}

<center><a href="https://www.facebook.com/groups/2637789973145014/" target="_blank">https://www.facebook.com/groups/2637789973145014/</a></center>
