---
type: pages
layout: canton
title: Canton de L'Isle-en-Dodon
url: /cantons/31230/canton-de-l'isle-en-dodon
aliases: []
date: "2021-09-01T22:16:34+02:00"
description: Groupe d'entre-aide et de partage sur le secteur de L' isle en Dodon.
  Joie de vivre ensemble solidaires.
resources: []
keywords:
- Haute-Garonne
- "31"
- L'Isle-en-Dodon
tags: []
regions:
- Haute-Garonne
- "31"
- L'Isle-en-Dodon
competences: []
participants: 20
social: []
friends: []
toc: true
lat: 43.38116
lon: 0.83662
---

## Présentation

Projets autour du partage de savoir-faire, de savoir-etre. Pour réinventer une vie plus souveraine, bienveillante et solidaire.

## Adresse

31230 L'Isle-en-Dodon
{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=1#/fiche/Canton-de-L'Isle-en-Dodon/n/" id="map" >}}
