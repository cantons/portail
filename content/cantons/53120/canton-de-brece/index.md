---
type: pages
layout: canton
title: Canton de Brecé
url: /cantons/53120/canton-de-brece
aliases: []
date: "2021-04-29T21:13:09+02:00"
description: Une vie simple , avec l'essentiel , l'échange des connaissances et des
  savoir faire ...
resources:
- name: featured
  src: image.jpg
keywords:
- "53"
- Mayenne
- Brecé
tags: []
regions:
- "53"
- Mayenne
- Brecé
competences: []
participants: 0
social: []
friends: []
toc: true
lat: 48.39879
lon: -0.78901
---
