---
title: "Permaculture"
description: ""
# date: 2021-04-20T00:00:00+02:00
keywords:
- Permaculture
- Agriculture
categories:
- Permaculture
- Agriculture
taxo: categories
toc: true
draft: true
---

<!-- Texte d'exemple provenant de Wikipédia -->

La [permaculture](https://fr.wikipedia.org/wiki/Permaculture) est un concept systémique et global qui vise à créer des écosystèmes respectant la biodiversité.
L'inspiration vient de la nature et de son fonctionnement (qui se nomme aussi biomimétisme ou écomimétisme).

C'est une science appliquée de conception de cultures, de lieux de vie, et de systèmes agricoles humains utilisant des principes d'écologie et le savoir des sociétés traditionnelles pour reproduire la diversité, la stabilité et la résilience des écosystèmes naturels.

{{< figure "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Jardin_en_permaculture.jpg/1280px-Jardin_en_permaculture.jpg" "Jardin en permaculture" "Jardin cultivé en permaculture dans le nord de la France" >}}

## Histoire

### Origine du mot

### Influences

### Mollison et Holmgren

## Éthique

## Principes

### Aménagement de l'espace

### Une méthode concrète

## Applications de la permaculture

### Agriculture

#### La forêt, une source d'inspiration majeure

#### Biodiversité

#### Agriculture de conservation, agroforesterie

#### Effet de bordure

#### Plantes vivaces

#### Animaux

#### Énergie

### Villes

### Économie

## Liens

- [Wikipédia](https://fr.wikipedia.org/wiki/Permaculture)
