---
type: pages
layout: map
title: "La carte des cantons"
description: "L'objectif des cantons est de réunir les individus souhaitant retrouver leur autonomie et revenir sur les vraies valeurs humaines, telles que la solidarité, l'entraide, l'échange, le partage d'idées, de travaux et de compétences."
keywords:
- Canton
- Carte
- GoGoCarto
# Hide map icon
icons: []
menu:
  main:
    identifier: map
    name: "Carte"
    title: "La carte des cantons"
    url: "/carte/"
    weight: 4
---

{{< iframe url="https://carte.cantons.info/annuaire?iframe=1&noheader=0" id="map" >}}
