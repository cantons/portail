---
{{ $slug := .TranslationBaseName -}}
title: "{{ replace $slug "-" " " | title }}"
subtitle: "Cantons Solidaires Autonomes"
date: {{ .Date }}
description: ""
keywords: []
tags: []
toc: false
author:
cantons: []
draft: true
---
