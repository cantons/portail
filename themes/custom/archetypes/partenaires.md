---
{{ $slug := path.Base .Dir -}}
{{ $name := replace $slug "-" " " | title -}}
title: "{{ $name }}"
date: {{ .Date }}
description: ""
link:
resources:
- name: featured
  src: {{ $slug }}.png
  title:
keywords: []
tags: []
toc: false
social: []
draft: true
---
