---
{{ $slug := path.Base .Dir -}}
{{ $name := replace $slug "-" " " | title -}}
{{ $dept := path.Base (path.Dir (trim .Dir "/")) -}}
{{ $prefix := "Canton" -}}
title: "{{ if not (hasPrefix $name $prefix) }}{{ $prefix }} {{ end }}{{ $name }}"
date: {{ .Date }}
description: ""
department: "{{ $dept }}"
aliases: []
resources:
- name: featured
  src: {{ $slug }}.jpg
  title: {{ $name }}
keywords:
- France
- Région
- {{ $name }}
- "{{ $dept }}"
tags:
- France
- Région
- {{ $name }}
- "{{ $dept }}"
toc: true
friends: []
social: []
draft: true
---

## Présentation

L'objectif des cantons est de réunir les individus souhaitant retrouver leur autonomie et revenir sur les vraies valeurs humaines, telles que la solidarité, l'entraide, l'échange, le partage d'idées, de travaux et de compétences.

Nous pourrons nous retrouver sur un jardin participatif et cultiver nos propres légumes et fruits, faire du troc, proposer des barbecues, pique-niquer et animer des ateliers recettes de façon à retrouver le lien social.

Unir nos forces pour vivre mieux.

Nous serons dans l'horizontalité et chacun(es) d'entre nous pourra donner ses idées et s'exprimer.

Il serait bien de pouvoir réunir des personnes qui s'intéressent à la protection de la Nature, la permaculture, aux animaux, à l'enseignement, à la santé, à la justice, à l'économie, etc.

Nous pourrons organiser des réunions sur le terrain ou en salle pour échanger nos idées et avancer ensemble dans l'autonomie et pourquoi pas utiliser la monnaie locale.

## Projets

### Premier projet

## Partenaires

{{< friends >}}
