window.addEventListener('DOMContentLoaded', () => {
  const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      const id = entry.target.getAttribute('id');
      const selector = `#TableOfContents ul li a[href="#${id}"]`;
      const el = document.querySelector(selector);
      if (!el) return; // Ignore missing nav item
      if (entry.intersectionRatio > 0) {
        el.classList.add('active');
      } else {
        el.classList.remove('active');
      }
    });
  });

  // Track all headers that have an `id` applied
  document.querySelectorAll('article h3[id], article h2[id]').forEach((section) => {
    observer.observe(section);
  });
});
