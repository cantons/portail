function main() {
    if (typeof groupName === 'undefined') {
        console.warn(`Mobilizon groupName is undefined`);
        return false;
    }
    if (typeof hostName === 'undefined') {
        console.warn(`Mobilizon hostName is undefined`);
        return false;
    }

    var baseURL = `https://${hostName}`;
    getEvents(groupName, baseURL);
}

function getEvents(groupName, baseURL) {
    // Replace this value to display the old events
    var afterDatetime = new Date(Date.now()).toISOString();
    //var afterDatetime = '2021-03-23T00:00:00Z';

    const query = `query {
    group (preferredUsername: "${groupName}") {
        id
        preferredUsername
        domain
        name
        organizedEvents (afterDatetime: "${afterDatetime}") {
            elements {
                uuid
                title
                beginsOn
            }
        }
    }
}`;

    fetch(`${baseURL}/graphiql`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({ query })
    })
    .then(r => r.json())
    .then(data => {
        console.log('Mobilizon data:', data);
        if (data && data.errors) {
            data.errors.forEach(e => console.error(e.message, e.locations));
            return false;
        }

        // TODO Handle empty data (no results message)
        data.data.group.organizedEvents.elements.sort(sortByDate).forEach(event => {
            getEvent(event.uuid, baseURL);
        });
    });
}

function sortByDate(event1, event2) {
    var date1 = new Date(event1.beginsOn);
    var date2 = new Date(event2.beginsOn);
    if (date1 < date2)
        return -1;
    if (date1 > date2)
        return -1;
    return 0;
}

function getEvent(uuid, baseURL) {
    const query = `query {
        event(uuid: "${uuid}") {
            id,
            uuid,
            url,
            title,
            beginsOn,
            endsOn,
            status,
            visibility,
            joinOptions,
            draft,
            picture {
                id
                url
                name
            },
            tags {
                slug,
                title
            }
        }
    }`;

    fetch(`${baseURL}/graphiql`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({ query })
    })
    .then(r => r.json())
    .then(data => {
        var event = data.data.event;
        displayEvent(event, baseURL);
    });
}

function displayEvent(event, baseURL) {
    var container = document.getElementById('mobilizon');
    var element = document.createElement('li');
    element.className = 'post';
    var beginsOn = new Date(event.beginsOn).toLocaleDateString('fr-FR', { weekday: 'long', day: 'numeric', month: 'long', year: 'numeric' });
    var dateStr = beginsOn.charAt(0).toUpperCase() + beginsOn.slice(1);

    var innerHTML = `<div class="post__header">`;

    if (event.picture) {
        innerHTML += `<a class="image-link featured-image circle-image" href="${baseURL}/events/${event.uuid}" target="_blank" rel="noreferrer noopener">
            <img src="${event.picture.url}" alt="${event.picture.name}"/>    
        </a>`;
    }

    innerHTML += `<div class="">
                <time class="post__date">${dateStr}</time>
                <h2 class="post__title"><a class="post-link" href="${baseURL}/events/${event.uuid}" target="_blank" rel="noreferrer noopener">${event.title}</a></h2>`;

    innerHTML += `<ul class="tags__list">`;
    if (event.tags) {
        event.tags.forEach(tag => {
            innerHTML += `<li class="tag__item">
                <a class="tag__link" href="${baseURL}/tag/${tag.title}" target="_blank" rel="noreferrer noopener">${tag.title}</a>
            </li>`;
        });
    }
    innerHTML += `</ul>`;
    innerHTML += `</div>
        </div>`;
    element.innerHTML = innerHTML;

    container.append(element);
}

main();
