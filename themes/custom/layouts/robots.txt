User-agent: *
{{ with .Site.Params.disallow -}}
{{ if eq (printf "%T" .) "string" }}
Disallow: {{ . }}
{{ else -}}
{{ range . }}
Disallow: {{ . }}
{{ end }}
{{- end }}
{{- end }}
Sitemap: {{ site.BaseURL }}sitemap.xml
